{-# language TypeApplications #-}
{-# language TemplateHaskell #-}
{-# language LambdaCase #-}
{-# language DeriveGeneric #-}

import Control.Lens (makeLenses, over, (^.), (&))
import System.Environment
import Data.List.Split (chunksOf)
import qualified Data.Vector as V
import Control.DeepSeq
import GHC.Generics

-- Types --

data Cell
  = Floor
  | Empty
  | Occupied
  deriving (Eq, Ord, Show, Generic)

instance NFData Cell

data Board
  = Board
  { _board :: V.Vector Cell
  , _width :: Int
  , _height :: Int
  }
  deriving (Eq, Generic)

instance NFData Board

data Point
  = Point
  { _x :: Int
  , _y :: Int
  }

makeLenses ''Board
makeLenses ''Point

instance Show Point where
  show p = "(" <> show (p ^. x) <> ", " <> show (p ^. y) <> ")"

-- Algorithm --

main :: IO ()
main = do
  myboard <- pure . toBoard . map (map toCell) . lines =<< readFile . head =<< getArgs
  let myboard' = steps myboard
  -- putStrLn $ ppBoard myboard
  -- putStrLn ""
  -- putStrLn $ ppBoard myboard'
  print $ V.length $ V.filter (==Occupied) (myboard' ^. board)

steps myboard =
  let
    myboard' = step myboard
  in
    -- without deepseq the whole board won't be evaluated
    -- which will cause a space leak TT_TT
    if deepseq myboard' myboard' == myboard
      then myboard
      else steps myboard'

step :: Board -> Board
step myboard =
  myboard
    & over board (V.imap $ updateCell myboard)

updateCell :: Board -> Int -> Cell -> Cell
updateCell myboard i cell =
  let
    point = indexToPoint i myboard
    -- use this instead for part 1
    -- adjacents = findAdjacents point myboard
    indir = findInDirection point myboard
  in
    case cell of
      Empty
        | all (/= Occupied) indir ->
          Occupied
      Occupied
--        | (>= 4) . length $ take 5 $ filter (== Occupied) adjacents ->
        | (>= 5) . length $ take 5 $ filter (== Occupied) indir ->
          Empty
      _ -> cell

-- key function for part 1
findAdjacents :: Point -> Board -> [Cell]
findAdjacents origin myboard =
  [ getCell myboard p
  | p <- map ($ origin)
      [ over x (+1)
      , over x (subtract 1)
      , over y (+1)
      , over y (subtract 1)
      , over x (+1) . over y (+1)
      , over x (+1) . over y (subtract 1)
      , over x (subtract 1) . over y (+1)
      , over x (subtract 1) . over y (subtract 1)
      ]
  , getCell myboard origin /= Floor
  , inBounds myboard p
  ]

-- key function for part 2
findInDirection :: Point -> Board -> [Cell]
findInDirection origin myboard =
      [ cell
      | cell <- concatMap ($ origin)
          [ dir myboard $ over x (+1)
          , dir myboard $ over x (subtract 1)
          , dir myboard $ over y (+1)
          , dir myboard $ over y (subtract 1)
          , dir myboard $ over x (+1) . over y (+1)
          , dir myboard $ over x (+1) . over y (subtract 1)
          , dir myboard $ over x (subtract 1) . over y (+1)
          , dir myboard $ over x (subtract 1) . over y (subtract 1)
          ]
      , getCell myboard origin /= Floor
      ]

dir :: Board -> (Point -> Point) -> Point -> [Cell]
dir myboard f =
  ( take 1
  . filter (/=Floor)
  . map (getCell myboard)
  . takeWhile (inBounds myboard)
  . drop 1 -- the first item is the original point, which we don't want.
  . iterate f
  )

inBounds :: Board -> Point -> Bool
inBounds myboard point =
  all id
    [ point ^. x >= 0
    , point ^. x < myboard ^. width
    , point ^. y >= 0
    , point ^. y < myboard ^. height
    ]

-- Utils --

indexToPoint :: Int -> Board -> Point
indexToPoint i myboard =
  Point (i `mod` myboard ^. width) (i `div` myboard ^. width)

getCell :: Board -> Point -> Cell
getCell myboard point =
  (myboard ^. board) V.! ((point ^. y * myboard ^. width) + point ^. x)

-- Parsing --

parseInput :: (String -> a) -> IO [a]
parseInput f = do
  pure . map f . lines =<< readFile . head =<< getArgs

toCell :: Char -> Cell
toCell = \case
  '.' -> Floor
  'L' -> Empty
  '#' -> Occupied

toChar :: Cell -> Char
toChar = \case
  Floor -> '.'
  Empty -> 'L'
  Occupied -> '#'

toBoard :: [[Cell]] -> Board
toBoard list =
  Board
    { _board = V.fromList $ concat list
    , _width = length (head list)
    , _height = length list
    }

ppBoard :: Board -> String
ppBoard myboard =
  ( unlines
  . chunksOf (myboard ^. width)
  . V.toList
  . V.map toChar
  ) (myboard ^. board)
