My solutions for AOC2020

Please note: the only good things about this code is that it is somewhat easy to write and that it works!

When building real world code avoid:

- Using partial functions
- Picking the wrong data structure for your algorithm
- Ignoring algorithms' complexity

But this is for fun and giggles so enjoy :)
