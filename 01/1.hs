import Data.List

main :: IO ()
main = do
  list <- map read . lines <$> readFile "input"
  let
    duplicatesMap =
      ( map (\(x:xs) -> (x, length xs + 1))
      . filter ((>1) . length)
      . group
      . sort
      ) list
  let
    products =
      [ x * y
      | x <- list, y <- list
      , x + y == 2020
      , if x == y 
        then ((>= 2) <$> lookup x duplicatesMap) == Just True
        else True
      ]
  mapM_ print products
