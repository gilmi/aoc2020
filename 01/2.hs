{-# language MultiWayIf #-}

import Data.List

main :: IO ()
main = do
  list <- map read . lines <$> readFile "input"
  let
    duplicatesMap =
      ( map (\(x:xs) -> (x, length xs + 1))
      . filter ((>1) . length)
      . group
      . sort
      ) list
  let
    products =
      [ x * y * z
      | x <- list, y <- list, z <- list
      , x + y + z == 2020
      , if
          | x == y, y /= z ->
            ((>= 2) <$> lookup x duplicatesMap) == Just True

          | x == z, y /= z ->
            ((>= 2) <$> lookup x duplicatesMap) == Just True

          | y == z, x /= z ->
            ((>= 2) <$> lookup y duplicatesMap) == Just True

          | y == z, x == z ->
            ((>= 3) <$> lookup x duplicatesMap) == Just True

          | otherwise ->
            True
      ]
  mapM_ print products
