{-# language LambdaCase #-}
{-# language ViewPatterns #-}

import System.Environment

type Map = [Line]
type Line = [Square]

data Square
  = Tree
  | Space
  | Hit
  | Safe

type PosX = Int
type PosY = Int

main :: IO ()
main = do
  mymap <- pure . toMap . lines =<< readFile . head =<< getArgs
  let
    results =
      map
        ( \slope ->
          process slope 0 mymap
        )
        [ (1, 1)
        , (3, 1)
        , (5, 1)
        , (7, 1)
        , (1, 2)
        ]

  -- mapM_ (putStrLn . map squareToChar) $ head results

  print
    . product
    . map sum
    . map
      ( concatMap $ map $ \case
        Hit -> 1
        _ -> 0
      )
    $ results

toMap :: [[Char]] -> Map
toMap =
  map $ map $ \case
    '#' -> Tree
    '.' -> Space
    c -> error $ "Unexpected character " <> show c <> ". Expecting '.' or '#'"

getIndex :: Int -> Line -> Square
getIndex i line =
  line !! (i `mod` length line)

process :: (Int, Int) -> PosX -> Map -> Map
process slope@(slopeX, slopeY) ((+) slopeX -> posX) (drop slopeY -> map') =
  case map' of
    nextLine : _ ->
      case getIndex posX nextLine of
        Tree ->
          concat
            [ take posX (cycle nextLine)
            , [Hit]
            , take 20 $ drop (posX + 1) (cycle nextLine)
            ]
          : process slope posX map'
        Space ->
          concat
            [ take posX (cycle nextLine)
            , [Safe]
            , take 20 $ drop (posX + 1) (cycle nextLine)
            ]
          : process slope posX map'
    [] -> []


squareToChar :: Square -> Char
squareToChar = \case
  Tree -> '#'
  Space -> '.'
  Hit -> 'X'
  Safe -> 'O'

