{-# language LambdaCase #-}
{-# language NamedFieldPuns #-}

import System.Environment
import Data.List.Split (splitOn)
import Text.Read (readMaybe)
import Data.Maybe (catMaybes)

data Info
  = Info
  { mytime :: Integer
  , buses :: [Maybe Integer]
  }

parse :: IO Info
parse = do
  [t, bs] <- fmap lines $ readFile . head =<< getArgs
  pure $ Info
    { mytime = read t
    , buses = (map readMaybe . splitOn ",") bs
    }

main :: IO ()
main = do
  Info{mytime, buses} <- parse
  let
    res = minimum $ map (\bus -> (bus - (mytime `mod` bus), bus)) (catMaybes buses)
  print res
  print $ uncurry (*) res
