{-# language LambdaCase #-}
{-# language TupleSections #-}
{-# language NamedFieldPuns #-}

import System.Environment
import Data.Foldable
import Data.List.Split (splitOn)
import Text.Read (readMaybe)
import Data.Maybe (catMaybes, fromJust)

data Info
  = Info
  { iMytime :: Int
  , iBuses :: [Maybe Int]
  }

parse :: IO Info
parse = do
  [t, bs] <- fmap lines $ readFile . head =<< getArgs
  pure $ Info
    { iMytime = read t
    , iBuses = (map readMaybe . splitOn ",") bs
    }

main :: IO ()
main = do
  Info{iBuses} <- parse
  let
    buses = catMaybes $ zipWith (\i b -> fmap (i,) b) [0..] iBuses
    res = part2 buses
  print res

-- Balantly copied from Justin Le because numbers theory is not my jam
-- https://github.com/mstksg/advent-of-code-2020/blob/master/reflections.md#day-13
--
-- Apparently for each two numbers we can calculate the "step" needed
-- to keep generating numbers that will work with our predicate
-- So we iteratively calculate that for the sequence of numbers.
part2 :: [(Int, Int)] -> Int
part2 = fst . foldl' findNext (0, 1)

findNext :: (Int, Int) -> (Int, Int) -> (Int, Int)
findNext (base, step) (offset, i) =
  let
    base' =
      iterateUntil
        (\n -> (n + offset) `mod` i == 0)
        (+ step)
        base
  in
    (base', step * i)

iterateUntil test f initial = 
  ( head
  . filter test
  . drop 1 -- the first element is the initial value
  . iterate f
  ) initial
