{-# language ViewPatterns #-}
{-# language LambdaCase #-}

import System.Environment
import Data.Foldable
import Debug.Trace

main = do
  moves <- parseInput parseLine
  print $ manhattan $ foldl' (mytrace step) (Pos (0, 0) (10, -1)) moves

mytrace :: (Pos -> Move -> Pos) -> (Pos -> Move -> Pos)
mytrace f pos move =
  let
    res = f pos move
  in
    trace (show move <> "\t => \t" <> show res) res

step (Pos loc@(x,y) wp@(wx, wy)) = \case
  MoveWaypoint wx' wy' ->
    Pos (x, y) (wx + wx', wy + wy')
  ToWaypoint num ->
    Pos (x + wx * num, y + wy * num) wp
  Turn dir ->
    Pos loc (rotatePoint wp dir)

manhattan (Pos (x,y) _) = abs x + abs y

data Pos
  = Pos Loc Waypoint
  deriving Show

type Loc
  = (Int, Int)

type Waypoint
  = (Int, Int)

data Move
  = MoveWaypoint Int Int
  | ToWaypoint Int
  | Turn Int
  deriving Show

data Facing
  = East
  | South
  | West
  | North
  deriving Show

parseInput :: (String -> a) -> IO [a]
parseInput f = do
  pure . map f . lines =<< readFile . head =<< getArgs

parseLine :: String -> Move
parseLine = \case
  'N':num ->
    dirToPoint (read num) North
  'S':num ->
    dirToPoint (read num) South
  'W':num ->
    dirToPoint (read num) West
  'E':num ->
    dirToPoint (read num) East
  'F':num ->
    ToWaypoint (read num)
  'L':num ->
    Turn (0 - read num)
  'R':num ->
    Turn (read num)

dirToPoint :: Int -> Facing -> Move
dirToPoint num = \case
  North ->
    MoveWaypoint 0 (0 - num)
  South ->
    MoveWaypoint 0 (0 + num)
  West ->
    MoveWaypoint (0 - num) 0
  East ->
    MoveWaypoint (0 + num) 0

rotatePoint (waypointX, waypointY) = \case
  90  -> (-waypointY, waypointX)
  180 -> (-waypointX, -waypointY)
  -90  -> (waypointY, -waypointX)
  -180 -> rotatePoint (waypointX, waypointY) 180
  -270 -> rotatePoint (waypointX, waypointY) 90
  270 -> rotatePoint (waypointX, waypointY) (-90)
  
