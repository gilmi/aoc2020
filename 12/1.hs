{-# language LambdaCase #-}

import System.Environment
import Data.Foldable
import Debug.Trace

main = do
  moves <- parseInput parseLine
  print $ manhattan $ foldl' (mytrace step) (Pos (0, 0) East) moves

mytrace :: (Pos -> Move -> Pos) -> (Pos -> Move -> Pos)
mytrace f pos move =
  let
    res = f pos move
  in
    trace (show move <> "\t => \t" <> show res) res

step (Pos (x,y) face) = \case
  Point x' y' -> Pos (x + x', y + y') face
  Turn dir num ->
    step (Pos (x, y) (applyDir dir face)) (dirToPoint num face)

manhattan (Pos (x,y) _) = abs x + abs y

data Pos
  = Pos (Int, Int) Facing
  deriving Show

data Move
  = Point Int Int
  | Turn Dir Int
  deriving Show

data Dir
  = F
  | L Int
  | R Int
  deriving Show

data Facing
  = East
  | South
  | West
  | North
  deriving Show

parseInput :: (String -> a) -> IO [a]
parseInput f = do
  pure . map f . lines =<< readFile . head =<< getArgs

parseLine :: String -> Move
parseLine = \case
  'N':num ->
    dirToPoint (read num) North
  'S':num ->
    dirToPoint (read num) South
  'W':num ->
    dirToPoint (read num) West
  'E':num ->
    dirToPoint (read num) East
  'F':num ->
    Turn F (read num)
  'L':num ->
    Turn (L $ read num `div` 90) 0
  'R':num ->
    Turn (R $ read num `div` 90) 0

dirToPoint :: Int -> Facing -> Move
dirToPoint num = \case
  North ->
    Point 0 (0 - num)
  South ->
    Point 0 (0 + num)
  West ->
    Point (0 - num) 0
  East ->
    Point (0 + num) 0

applyDir :: Dir -> Facing -> Facing
applyDir dir facing =
  case dir of
    F -> facing
    L num -> iterate goLeft facing !! num
    R num -> iterate goRight facing !! num

goRight = \case
  East -> South
  South -> West
  West -> North
  North -> East

goLeft = \case
  North -> West
  West -> South
  South -> East
  East -> North
