{-# language LambdaCase #-}

main :: IO ()
main = do
  passdescs <- map parse . lines <$> readFile "input"
  let
    good =
      [ desc
      | desc@((pos1, pos2), char, password) <- passdescs
      , let
          amount =
            length . concat $
              [ [ () | Just char == getIndex pos1 password ]
              , [ () | Just char == getIndex pos2 password ]
              ]
        in
          amount == 1
      ]
  mapM_ print good
  print $ length good

parse :: String -> ((Int, Int), Char, String)
parse line =
  let
    [policy1, policy2, password] = words line
  in
    ( ( read $ takeWhile (/='-') policy1
      , read $ drop 1 $ dropWhile (/='-') policy1
      )
    , head policy2
    , password
    )

getIndex :: (Eq t, Num t) => t -> [a] -> Maybe a
getIndex i = \case
  [] -> Nothing
  x : xs
    | i == 1 -> Just x
    | otherwise -> getIndex (i - 1) xs
