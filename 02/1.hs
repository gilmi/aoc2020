main :: IO ()
main = do
  passdescs <- map parse . lines <$> readFile "input"
  let
    good =
      [ password
      | ((low, high), char, password) <- passdescs
      , let
          len = count char password
        in
          low <= len && len <= high
      ]
  print $ length good

parse :: [Char] -> ((Int, Int), Char, String)
parse line =
  let
    [policy1, policy2, password] = words line
  in
    ( ( read $ takeWhile (/='-') policy1
      , read $ drop 1 $ dropWhile (/='-') policy1
      )
    , head policy2
    , password
    )

count :: (Num c, Eq a) => a -> [a] -> c
count c = sum . map (\s -> if s == c then 1 else 0)
