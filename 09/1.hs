{-# language TypeApplications #-}
{-# language ViewPatterns #-}
{-# language LambdaCase #-}

import Data.List
import Data.Bool
import System.Environment

parseInput :: (String -> a) -> IO (Int, [a])
parseInput f = do
  ((read -> num) : file : _) <- getArgs
  pure . (,) num . map f . lines =<< readFile file

main :: IO ()
main = do
  (num, input) <- parseInput @Int read
  let
    grouped = mygroup num input
    Left invalid = mapM_ check grouped
    list = calc invalid input
  print (maximum list + minimum list)

calc :: Int -> [Int] -> [Int]
calc target = \case
  x : xs ->
    maybe (calc target xs) (x:) (calcStep (target - x) xs)

calcStep :: Int -> [Int] -> Maybe [Int]
calcStep target = \case
  x : xs
    | x == target ->
      pure [x]
    | x < target ->
      (:) x <$> calcStep (target - x) xs
  _ ->
      Nothing

check :: (Int, [Int]) -> Either Int ()
check (num, pre) =
  bool (Left num) (Right ()) $ any (\(a,b) -> num == a + b) (pairs pre)

pairs :: [Int] -> [(Int, Int)]
pairs list =
  let
    duplicatesMap =
      ( map (\(x:xs) -> (x, length xs + 1))
      . filter ((>1) . length)
      . group
      . sort
      ) list
  in
    [ (x, y)
    | x <- list, y <- list
    , if x == y 
      then ((>= 2) <$> lookup x duplicatesMap) == Just True
      else True
    ]

mygroup :: Int -> [Int] -> [(Int, [Int])]
mygroup num list =
  case (take num list, drop num list) of
    (_, []) ->
      []

    (preemptives, r:_) ->
      (r, preemptives) : mygroup num (drop 1 list)

