{-# language ViewPatterns #-}
{-# language LambdaCase #-}

import Data.Char (isDigit)
import Text.Read (readMaybe)
import Control.Monad ((<=<))
import qualified Data.Map as M
import Data.List (groupBy)
import System.Environment (getArgs)

main :: IO ()
main = do
  entries <- pure . parse =<< readFile . head =<< getArgs
  let
    good =
      filter $ \entry ->
        all
          ( \(field, validate) ->
            (==) (Just True) (validate =<< M.lookup field entry)
          )
          fields
  mapM_ print (good entries)
  print $ length (good entries)

fields :: [(String, String -> Maybe Bool)]
fields =
  [ ("byr", pure . (`elem` [1920..2002]) <=< readMaybe)
  , ("iyr", pure . (`elem` [2010..2020]) <=< readMaybe)
  , ("eyr", pure . (`elem` [2010..2030]) <=< readMaybe)
  , ("hgt"
    , \(break (not . isDigit) -> (number, unit)) ->
        case unit of
          "cm" -> (`elem` [150..193]) <$> readMaybe number
          "in" -> (`elem` [59..76]) <$> readMaybe number
          _ -> Nothing
    )
  , ("hcl"
    , \case
      '#' : hex -> Just $ length hex == 6 && all (`elem` ['0'..'9'] <> ['a'..'f']) hex
      _ -> Nothing
    )
  , ("ecl", Just . (`elem` ["amb", "blu", "brn", "gry", "grn", "hzl", "oth"]))
  , ("pid", \pid -> Just $ length pid == 9 && all isDigit pid)
  -- , "cid"
  ]

parse :: String -> [M.Map String String]
parse =
  ( map
    ( fmap (drop 1)
    . M.fromListWith error
    . map (break (==':'))
    . words
    . unwords
    )
  . groupBy (\l1 l2 -> "" `notElem` [l1, l2])
  . lines
  )
