{-# language LambdaCase #-}

import System.Environment

main :: IO ()
main = do
  codes <- fmap lines $ readFile . head =<< getArgs
  let
    seats = map (findSeat (0, 127) (0, 7)) codes
    seatIds = map seatId seats
    missingSeat =
      [ seatId (row, col)
      | row <- [0..127]
      , col <- [0..7]
      , seatId (row, col) `notElem` seatIds
      , seatId (row, col) + 1 `elem` seatIds
      , seatId (row, col) - 1 `elem` seatIds
      ]
  print missingSeat

findSeat :: (Int, Int) -> (Int, Int) -> [Char] -> (Int, Int)
findSeat rowRange@(lowRow, highRow) colRange@(lowCol, highCol) = \case
  [] ->
    (lowRow, lowCol)
  c : rest ->
    case c of
      'F' -> findSeat (lowRow, (highRow + lowRow) `div` 2) colRange rest
      'B' -> findSeat ((highRow + lowRow + 1) `div` 2, highRow) colRange rest
      'L' -> findSeat rowRange (lowCol, (highCol + lowCol) `div` 2) rest
      'R' -> findSeat rowRange ((highCol + lowCol + 1) `div` 2, highCol) rest

seatId :: Num a => (a, a) -> a
seatId (row, col) = row * 8 + col
