{-# language ViewPatterns #-}
{-# language LambdaCase #-}

import System.Environment
import Data.Bool

main :: IO ()
main = do
  bags <- pure . map parse . lines =<< readFile . head =<< getArgs
  mapM_ print bags
  print $ sum $ map (\(bag, _) -> bool 0 1 $ travel bag "shiny gold" bags) bags

parse :: String -> (String, [(Int, String)])
parse = \case
  (words -> c1:c2:"bags":"contain":"no":_) ->
    (unwords [c1,c2], [])
  (words -> c1:c2:"bags":"contain":rest) ->
    (unwords [c1,c2], items rest)

items :: [String] -> [(Int, String)]
items = \case
  [] -> []
  a:b:c:_:rest ->
    (read a, unwords [b,c]) : items rest

travel :: String -> String -> [(String, [(Int, String)])] -> Bool
travel from to graph =
  case lookup from graph of
    Nothing ->
      False
    Just list ->
      any ((==) to . snd) list || any (\(_, newFrom) -> travel newFrom to graph) list
