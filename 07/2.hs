{-# language ViewPatterns #-}
{-# language LambdaCase #-}

import System.Environment

main :: IO ()
main = do
  bags <- pure . map parse . lines =<< readFile . head =<< getArgs
  mapM_ print bags
  print $ countBags "shiny gold" bags

parse :: String -> (String, [(Int, String)])
parse = \case
  (words -> c1:c2:"bags":"contain":"no":_) ->
    (unwords [c1,c2], [])
  (words -> c1:c2:"bags":"contain":rest) ->
    (unwords [c1,c2], items rest)

items :: [String] -> [(Int, String)]
items = \case
  [] -> []
  a:b:c:_:rest ->
    (read a, unwords [b,c]) : items rest

countBags :: String -> [(String, [(Int, String)])] -> Int
countBags from graph =
  case lookup from graph of
    Nothing ->
      error "expecting to find any bag!"
    Just list ->
      sum $
        map
          (\(num, newFrom) -> num + (num * countBags newFrom graph))
          list
