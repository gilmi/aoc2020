{-# language LambdaCase #-}
{-# language TemplateHaskell #-}

import qualified Data.Vector as V
import Control.Lens
import System.Environment
import Control.Monad

type Program
  = V.Vector Op

data Op
  = Nop
  | Acc Int
  | Jmp Int
  deriving Show

data Machine
  = Machine
  { _accumulator :: Int
  , _program :: Program
  , _ip :: Int
  , _visited :: [Int]
  }
  deriving Show

makeLenses ''Machine

main = do
  machine <- pure . initMachine . V.fromList . map parseOp . lines =<< readFile . head =<< getArgs
  print $ foldM (flip ($)) machine (cycle [evalOp])

initMachine :: Program -> Machine
initMachine prog =
  Machine 0 prog 0 mempty

parseInt :: String -> Int
parseInt = \case
  '+':num -> read num
  num -> read num

parseOp :: String -> Op
parseOp = \case
  'n':'o':'p':' ':num ->
    Nop
  'a':'c':'c':' ':num ->
    Acc $ parseInt num
  'j':'m':'p':' ':num ->
    Jmp $ parseInt num

evalOp :: Machine -> Either Int Machine
evalOp machine
  | _ip machine `elem` _visited machine =
    Left $ _accumulator machine
  | otherwise =
    case _program machine V.! _ip machine of
      Nop ->
        pure $ machine
          & over ip (+1)
          & over visited ((:) $ _ip machine)
      Acc i ->
        pure $ machine
          & over ip (+1)
          & over accumulator (+i)
          & over visited ((:) $ _ip machine)
      Jmp n ->
        pure $ machine
          & over ip (+n)
          & over visited ((:) $ _ip machine)



