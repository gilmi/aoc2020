{-# language LambdaCase #-}
{-# language ViewPatterns #-}
{-# language TemplateHaskell #-}

import qualified Data.Vector as V
import Control.Lens
import System.Environment
import Control.Monad

type Program
  = V.Vector Op

data Op
  = Nop Int
  | Acc Int
  | Jmp Int
  deriving Show

data Machine
  = Machine
  { _accumulator :: Int
  , _program :: Program
  , _ip :: Int
  , _visited :: [Int]
  , _changedOp :: Int
  }
  deriving Show

makeLenses ''Machine

main = do
  machine <- pure . initMachine . V.fromList . map parseOp . lines =<< readFile . head =<< getArgs
  print $ foldM apply (M $ machine (-1)) (cycle [evalOp])

initMachine :: Program -> Int -> Machine
initMachine prog = Machine 0 prog 0 mempty

parseInt :: String -> Int
parseInt = \case
  '+':num -> read num
  num -> read num

parseOp :: String -> Op
parseOp = \case
  'n':'o':'p':' ':num ->
    Nop $ parseInt num
  'a':'c':'c':' ':num ->
    Acc $ parseInt num
  'j':'m':'p':' ':num ->
    Jmp $ parseInt num

data Result
  = M Machine
  | L Machine
  | I Int
  deriving Show

evalOp :: Result -> Result
evalOp (M machine)
  | _ip machine `elem` _visited machine =
    L machine
  | otherwise =
    case _program machine V.!? _ip machine of
      Just (Nop _) ->
        M $ machine
          & over ip (+1)
          & over visited ((:) $ _ip machine)
      Just (Acc i) ->
        M $ machine
          & over ip (+1)
          & over accumulator (+i)
          & over visited ((:) $ _ip machine)
      Just (Jmp n) ->
        M $ machine
          & over ip (+n)
          & over visited ((:) $ _ip machine)
      Nothing ->
        I $ _accumulator machine

findNextOp :: Int -> Program -> (Int, Op)
findNextOp ((+1) -> i) prog =
  case prog V.!? i of
    Nothing -> error $ show i
    Just op ->
      case op of
        Acc{} -> findNextOp i prog
        Nop n -> (i, Jmp n)
        Jmp n -> (i, Nop n)

apply :: Result -> (Result -> Result) -> Either Int Result
apply m f = case m of
  I i ->
    Left i
  L machine -> do
    let
      oldop
        | _changedOp machine == (-1) =
          []
        | otherwise =
          [ ( _changedOp machine
            , case _program machine V.! _changedOp machine of
              Nop n -> Jmp n
              Jmp n -> Nop n
            )
          ]
      newop = findNextOp (_changedOp machine) (_program machine)
    pure $ M $ initMachine (_program machine V.// (oldop <> [newop])) (fst newop)
  M machine ->
    pure $ f $ M machine

