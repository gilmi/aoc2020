import System.Environment
import Data.List
import Data.Bool

main :: IO ()
main = do
  content <- readFile . head =<< getArgs
  -- print . sum . map (length . nub . concat) . groupBlankline . lines $ content
  print . sum . map solve . groupBlankline . lines $ content

groupBlankline = groupBy (\l1 l2 -> "" `notElem` [l1, l2])

solve :: [String] -> Int
solve answers =
  sum $ map (\q -> bool 0 1 $ all (elem q) answers) ['a'..'z']
