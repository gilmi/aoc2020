{-# language TypeApplications #-}

import Control.Arrow
import Data.List
import System.Environment

parseInput :: (String -> a) -> IO [a]
parseInput f = do
  pure . map f . lines =<< readFile . head =<< getArgs

main = do
  input <- sort . (:) 0 <$> parseInput @Int read
  -- print input
  print
    . uncurry (*)
    . (sum *** sum)
    . unzip
    . (:) (0, 1)
    . map
      ( \(a, b) ->
         case b - a of
           1 -> (1, 0)
           3 -> (0, 1)
           _ -> (0, 0)
      )
    $ zip input (tail input)
