{-# language TypeApplications #-}
{-# language LambdaCase #-}

import Data.List
import Data.Traversable
import System.Environment
import Control.Concurrent

parseInput :: (String -> a) -> IO [a]
parseInput f = do
  pure . map f . lines =<< readFile . head =<< getArgs

main = do
  input <- (\xs -> xs <> [maximum xs + 3]) . sort . (:) 0 <$> parseInput @Int read
  mvars <- for input $ \x -> (,) x <$> newEmptyMVar
  mapM_ (forkIO . solve) (tails mvars)
  print =<< readMVar (snd $ head mvars)

solve :: [(Int, MVar Integer)] -> IO ()
solve = \case
  [] -> pure ()
  [(_, mvar)] -> putMVar mvar 1
  ((x0, x0mvar) : rest) -> do
    results <- sequence
      [ readMVar xmvar
      | (x, xmvar) <- rest
      , x - x0 <= 3
      ]
    putMVar x0mvar (sum results)

